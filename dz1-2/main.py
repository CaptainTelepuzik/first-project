import calendar


# Задание 1

def date_number(day: int, month: int, year: int):
    months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
              'ноября', 'декабря']
    months_not_31 = [4, 6, 9, 11]

    if (type(day) != int) or (type(month) != int) or (type(year) != int):
        return 'Неверно введенный тип данных'
    elif day > 31 or day <= 0:
        return 'Неверное количество дней!'
    elif month in months_not_31:
        if day > 30:
            return 'В этом месяцу нет 31 день!'
        else:
            return f'{day} {months[month - 1]} {year} года'
    # Проверка февраля
    elif month == 2:
        # Проверка високосного года
        if calendar.isleap(year) != True:
            if (day > 28):
                return 'В этом году 28 дней в феврале'
            else:
                return f'{day} {months[month - 1]} {year} года'
        elif calendar.isleap(year):
            if (day > 29):
                return 'В этом году 29 дней в феврале'
            else:
                return f'{day} {months[month - 1]} {year} года'
        else:
            return f'{day} {months[month - 1]} {year} года'
    elif month > 12 or month <= 0:
        return 'Неверно введено количество месяцев в году'
    elif (year <= 0):
        return 'Неверно введен год'
    else:
        return f'{day} {months[month - 1]} {year} года'


# print(date_number(29, 2, 2024))

# Задание 2

def name_counter(names):
    if type(names) is tuple:
        return {i: names.count(i) for i in names}
    else:
        return "Не верный формат!"


# print(name_counter(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')))

# Задание 3

def fio_people(people):
    if type(people) is dict:
        if 'last_name' not in people and 'first_name' in people and 'middle_name' in people:
            return people['first_name'] + ' ' + people['middle_name']
        elif 'middle_name' not in people:
            if 'first_name'not in people:
                return people['last_name']
            else:
                return people['last_name'] + ' ' + people['first_name']
        elif 'middle_name' not in people and 'last_name' in people:
            return people['last_name'] + ' ' + people['first_name']
        elif 'first_name' not in people:
            return people['last_name']
        elif 'last_name' not in people and 'first_name' not in people or 'middle_name' not in people:
            return "Нет данных!"
        else:
            return people['last_name'] + ' ' + people['first_name'] + ' ' + people['middle_name']
    else:
        return 'Неверный тип данных!'


#print(fio_people({'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': 'Иванович'}))


# Задание 4

def digit_easy(digit):
    if type(digit) is int:
        for i in range(2, digit):
            if digit % i == 0:
                return False
        return True
    else:
        return "Не верный формат!"
# print(digit_easy(10))

# Задание 5

def unique_digit(*args):

    digit = []

    for i in args:
        if isinstance(i, int) or isinstance(i, float):
            if i not in digit:
                digit.append(i)

    digit.sort()

    return digit

# print(unique_digit(1, '2', 'text', 42, None, None, None, 15, True, 1, 1))
