from django.contrib import admin

from clients.models import Client, Address, Contract, Service


@admin.register(Client)
class Client(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'telephone')


@admin.register(Address)
class Address(admin.ModelAdmin):
    list_display = ('street', 'home', 'flat')


@admin.register(Service)
class Service(admin.ModelAdmin):
    list_display = ('name', 'price')


@admin.register(Contract)
class Contract(admin.ModelAdmin):
    list_display = ('client', 'address', 'is_active')
