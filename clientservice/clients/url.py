from django.urls import path

from clients.views import Homepage, Info, Clients, Services, Contracts

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('info/', Info.as_view(), name='info'),
    path('client/', Clients.as_view(), name='client'),
    path('services/', Services.as_view(), name='services'),
    path('contracts/', Contracts.as_view(), name='contracts'),
]