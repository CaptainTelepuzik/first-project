from django.db import models


class Client(models.Model):
    first_name = models.CharField('имя', max_length=50, blank=True)
    last_name = models.CharField('фамилия', max_length=50, blank=True)
    middle_name = models.CharField('отчество', max_length=50, blank=True)
    telephone = models.CharField('телефон', max_length=50, blank=True)
    dc = models.DateField('дата создания', auto_now_add=True)

    def __str__(self):
        return self.get_full_name_str()

    def get_full_name_str(self) -> str:
        last_name = self.last_name
        first_name = self.first_name
        middle_name = self.middle_name

        if first_name:
            return f'{last_name} {first_name} {middle_name}'.strip()
        elif last_name:
            return last_name
        else:
            return 'Нет данных'

class Address(models.Model):
    city = models.CharField('город', max_length=50, blank=True)
    street = models.CharField('улица', max_length=50, blank=True)
    home = models.CharField('дом', max_length=50, blank=True)
    building = models.CharField('корпус', max_length=50, blank=True)
    flat = models.CharField('квартира', max_length=50, blank=True)

    def __str__(self):
        return self.street + self.home + self.building


class Service(models.Model):
    name = models.CharField('Наименование услуги', max_length=50)
    description = models.TextField('описание', blank=True)
    price = models.CharField('Стоимость услуги', max_length=20, blank=True)
    dedline = models.DateField('Сроки выполнения', null=True)
    dc = models.DateField('дата создания', auto_now_add=True)

    def __str__(self):
        return self.name



class Contract(models.Model):
    client = models.ForeignKey(Client, verbose_name='клиент', on_delete=models.CASCADE, related_name='contracts')
    address = models.ForeignKey(Address, verbose_name='адрес заказа', on_delete=models.CASCADE,
                                related_name='contracts')
    service = models.ForeignKey(Service, verbose_name='услуга', on_delete=models.CASCADE, related_name='contracts')
    is_active = models.BooleanField('Статус договора', default=False)
    is_complited = models.BooleanField('Завершен', default=False)
    dc = models.DateField('дата создания', auto_now_add=True)

    def __str__(self):
        return f'Договор клиента {str(self.client)}'

    def get_is_complited(self):
        if self.is_complited==True:
            return 'Заключен'
        else:
            return 'Не заключен'
