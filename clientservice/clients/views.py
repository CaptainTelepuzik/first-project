from django.shortcuts import render
from django.views import View

from clients import models


class Homepage(View):
    def get(self, request):
        context = {'text':'Веб-приложение "Мои клиенты"!', 'title': 'Мои клиенты'}
        return render(request=request, template_name='clients/mainpage.html', context=context)

class Info(View):
    def get(self, request):
        context = {'title': 'Информация о проекте'}
        return render(request=request, template_name='clients/info.html', context=context)

class Clients(View):
    def get(self, request):
        clients = models.Client.objects.all()
        context = {'title': 'Клиенты', 'clients': clients}
        return render(request=request, template_name='clients/client.html', context=context)

class Services(View):
    def get(self, request):
        services = models.Service.objects.all()
        context = {'title': 'Услуги', 'services': services}
        return render(request=request, template_name='clients/services.html', context=context)

class Contracts(View):
    def get(self, request):
        contracts = models.Contract.objects.all()
        context = {'title': 'Контракты', 'contracts': contracts}
        return render(request=request, template_name='clients/contracts.html', context=context)


